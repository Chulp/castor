<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2025 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php

/* module id*/ 
$module_name = 'xsetupa';
$version = '20250118';
$main_file = "castor";
$default_template = '/back.lte';

/* start initialize module */
global $oLEPTON;
$oFC = gsmoffa::getInstance();
$oTWIG = lib_twig_box::getInstance ( );
$oTWIG-> registerModule ( LOAD_MODULE . LOAD_SUFFIX );
$template_name= '@' . LOAD_MODULE . LOAD_SUFFIX . "/". LANGUAGE . $default_template;

/* version data */
$oFC->version [ $module_name ] = $version;
$oFC->version = array_merge ( $oFC->version, $version_display);
$oFC->version [ $oFC-> language [ 'LANG' ] ] = $oFC-> language [ 'VERS' ];
$oFC->version [ "set" ] = $FC_SET [ 'version' ] ;
$project= sprintf ("%s %s " , $oFC-> language [ 'TXT_SETUP' ], strtoupper ( $main_file )) ;

/* file references */
$oFC->file_ref  [ 99 ] = LOAD_DBBASE . "_".$main_file;

/* settings */
$oFC->setting [ 'includes' ] 		= $place [ 'includes' ];
$oFC->setting [ 'frontend' ] 		= $place [ 'frontend' ];
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET );
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "zoek" );
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "entity");
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "droplet");

/* default values */
$oFC->page_content [ 'FORMULIER' ] = $project;
$oFC->page_content [ 'MODULE' ] = $module_name;
$oFC->page_content [ 'MODE' ] = 9;
$oFC->page_content [ 'PAGE_ID' ] = $page_id ?? 0;
$oFC->page_content [ 'SECTION_ID' ] = $section_id ?? 0;

/* Gebruik limited door rechthebbenden */
if ( $oFC->user [ 'privileged' ] > 0 ) {
	$oFC->user  = array_merge (	$oFC->user, $oFC->gsm_adresDet ( $oFC->page_content [ 'PAGE_ID' ], $oFC->setting [ 'owner' ] ) );
	if ($oFC->user[ 'privileged' ] > 2 ) {
		$oFC->page_content [ 'MODE' ] = 9;
		$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET );
	}
	/* niet genoeg rechten * /
	if ( $oFC->page_content [ 'MODE' ] == 0 ) {
		$oFC->description = $oFC->language [ 'TXT_NO_ACCESS' ];
		unset ( $_POST[ 'command' ] );
	}
	/* end genoeg rechten */
}
/* create condition for sips test */
$_SESSION[ 'page_h' ] = $oFC->page_content ['HASH'];

/* get memory values */ 
$oFC->gsm_memorySaved ( );

if ($oFC->setting [ 'debug' ] == "yes" ) Gsm_debug (array ("post"=> $_POST, "get"=>$_GET ?? "", "this"=>$oFC, $selection ?? ""), __LINE__ . __FUNCTION__ ); 

/* selection */
$oFC->search_mysql = "";
if ( isset ( $selection ) && strlen ( $selection ) > 1 ) {
	$help = "%" . str_replace ( ' ', '%', str_replace ( "?", "", trim ( $selection ) ) ) . "%";
	$oFC->search_mysql .= " WHERE `zoek` LIKE '" . $help . "'";
} else { 
	$selection = "";
}	

/* sips test before job */ 
if ( isset( $_POST[ 'command' ] ) && $oFC->sips) { 
	unset ($_POST); 
	$oFC->description .= date('G:i:s'.substr ( ( string ) microtime ( ), 1, 8).' : ') . __LINE__  .
		$oFC->language [ 'TXT_ERROR_SIPS' ] . NL; 
}

/* Input processing */
if ( isset( $_POST[ 'command' ] ) ) {
	switch ( $_POST[ 'command' ] ) {
		case "up":
			$oFC->page_content [ 'POSITION' ] = $oFC->gsm_pagePosition ("up", $oFC->page_content [ 'POSITION' ] , $_POST[ 'n2' ],  $oFC->setting [ 'qty_max' ], $_POST[ 'n0' ], $_POST[ 'n1' ] );
			break;
		case "down":
			$oFC->page_content [ 'POSITION' ] = $oFC->gsm_pagePosition ("down", $oFC->page_content [ 'POSITION' ] , $_POST[ 'n2' ], $oFC->setting [ 'qty_max' ], $_POST[ 'n0' ], $_POST[ 'n1' ] );
			break;
		case "Select":
		case "View":
			if ( !isset( $_POST[ 'vink' ][ 0 ] ) ) 	break;
			$oFC->page_content [ 'MODE' ] = 8;
			$oFC->recid = $_POST[ 'vink' ][ 0 ];
			break;
		case "Reset":
			$oFC->recid = '';
			$oFC->search_mysql = "";
			$selection= "";
			$oFC->page_content [ 'PARAMETER' ] = $selection;
//			$oFC->selection = $selection;
			$oFC->page_content [ 'SUB_HEADER' ]= "____";
		default:
			$oFC->page_content [ 'MODE' ] = 9;
			break;
	} 
} elseif ( isset( $_GET[ 'command' ] ) ) {
	switch ( $_GET[ 'command' ] ) {
		case 'select': //    1=>'Wijzigen', 
			// is a record selected ? 
			if ($oFC->recid == "") { 
				$oFC->page_content [ 'MODE' ] = 9; 
				$oFC->description .= $oFC->MOD_GSMOFF['MSG_NO_DATA'];
				break;
			} 
			$oFC->recid = $_GET[ 'recid' ];
			$oFC->page_content [ 'MODE' ] = 8; 
			break;
		default:
			// escape route 
			$oFC->page_content [ 'MODE' ] = 9;
			break;
	} //$_GET[ 'command' ]
} else {
	/* first run */
	$oFC->page_content [ 'P1' ] = true;
	$oFC->page_content [ 'MODE' ] = 9;

	/* Default settings */
	$job = array ();
	
	/* check if upload set for file types is present */
	$results = array ( );
	$database->execute_query ( 
		sprintf ( "SELECT * FROM `%smod_go_taxonomy` WHERE `type` = 'setting' AND `ref` = '%s'", 
			TABLE_PREFIX, 
			"allowed|gsmoffa|2" ) , 
		true, 
		$results );
	if ( count ( $results ) == 0 ) { 
		$job [] = sprintf ( "INSERT INTO `%smod_go_taxonomy` ( `type`, `ref`, `name`, `active`) 
			VALUES ('setting', '%s', '%s', '1' )",
			TABLE_PREFIX , 
			"allowed|gsmoffa|2", 
			"jpg|html|zip|pdf|mp4" );
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' setting allowed  added  ' . NL;
	}
	
	if ( !isset ( $oFC->setting [ 'remove' ] ) ) {
		$main_parameter = 'recycle';
		$job [] = sprintf ( "INSERT INTO `%smod_go_taxonomy` ( `type`, `ref`, `name`, `active`) 
			VALUES ('setting', '%s', '%s', '1' )",
			TABLE_PREFIX, 
			'remove', 
			$main_parameter);
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' remove keyword setting added ' . $main_parameter . NL;
		$oFC->setting [ 'remove' ] = $main_parameter;
	}

	if ( !isset ( $oFC->setting [ 'zoek' ] [ $main_file ] ) ) {
		$job [] = sprintf ( "INSERT INTO `%smod_go_taxonomy` ( `type`, `ref`, `name`, `active`) 
			VALUES ('zoek', '%s', '%s', '1' )",
			TABLE_PREFIX, 
			$main_file, 
			'|type|ref|');
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' zoek schema  added  ' . $main_file . NL;
		$oFC->setting [ 'zoek' ] [ 'castor' ] = "|type|ref|";
	}
	if (!isset ( $oFC->setting [ 'mediadir' ] ) ) {
		$main_parameter = 'mediadir|gsmoffa';
		$job [] = sprintf ( "INSERT INTO  `%smod_go_taxonomy` ( `type`, `ref`, `name`, `active`) 
			VALUES ('setting', '%s', '%s', '1' )",
			TABLE_PREFIX, 
			$main_parameter, 
			'/media/archive');
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' directory setting ' . 'mediadir' . NL;
		$oFC->setting [ 'mediadir' ] = '/media/archive';
	}
	if (!isset ( $oFC->setting [ 'datadir' ] ) ){
		$main_parameter = 'datadir|gsmoffa';
		$job [] = sprintf ( "INSERT INTO   `%smod_go_taxonomy` ( `type`, `ref`, `name`, `active`) 
			VALUES ('setting', '%s', '%s', '1' )",
			TABLE_PREFIX, 
			'datadir|gsmoffa', 
			'/castor' );
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' directory setting ' . 'datadir' . NL;
		$oFC->setting [ 'datadir' ] = '/castor';
	}

	/* achtual upgrade */
	if ( isset ( $job ) && count( $job ) > 0 ) {
		foreach( $job as $key => $query ) {
			$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' ' . $query . NL;
			$database->simple_query ( $query ); 
		} 
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' Default setting added' .  NL; 
	}
	/* upgraded */

	/* create directories */
	$oFC->gsm_existDir ( sprintf ('%s%s' ,LEPTON_PATH , $oFC->setting [ 'mediadir' ] ), true  );
	$oFC->gsm_existDir ( sprintf ('%s%s' ,LEPTON_PATH , $oFC->setting [ 'datadir' ] ), true  );	
	
	/* create databases  1 */
	$oFC->description .= $oFC->gsm_existDb ( $oFC->file_ref  [ 99 ] );
	
	/* check which fields are present in the main file */	
	$result = array ( );
	$database->execute_query ( sprintf ( 'DESCRIBE %s', $oFC->file_ref [ 99 ] ),
		true, 
		$result );
	
	/* add fields not present / change fields*/
	$localHulpA = array();
	foreach ( $result as $row ) $localHulpA [$row [ 'Field' ]] = $row [ 'Type' ];
	
	$job = array ();
	if ( !isset ( $localHulpA[ 'active' ] ) ) {  // gewoon toevoegen
		$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `active` int(3) NOT NULL DEFAULT '1' AFTER `zoek`", $oFC->file_ref [ 99 ] );
	}
	if ( isset ( $localHulpA[ 'prefix' ] ) ) {  // wijzigen	
		$job [ ] = sprintf ( "ALTER TABLE `%s` CHANGE `prefix` `type` varchar(63) NOT NULL DEFAULT ''", $oFC->file_ref [ 99 ] );
	}
	if ( isset ( $localHulpA[ 'project' ] ) ) {  // wijzigen	
		$job [ ] = sprintf ( "ALTER TABLE `%s` CHANGE `project` `ref` varchar(63) NOT NULL DEFAULT ''", $oFC->file_ref [ 99 ] );
	}
	if ( isset ( $localHulpA[ 'aant' ] ) ) {  // wijzigen
		$job [ ] = sprintf ( "ALTER TABLE `%s` CHANGE `aant` `content_short` text", $oFC->file_ref [ 99 ] );
		$job [ ] = sprintf ( "ALTER TABLE `%s` CHANGE `content_short` `content_short`  VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT ''", $oFC->file_ref [ 99 ] );

	}		
	if ( !isset ( $localHulpA[ 'keeptill' ] ) ) {  // toevoegen
		$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `keeptill` char(16) NOT NULL DEFAULT '' AFTER `name`", $oFC->file_ref [ 99 ] );
	}	
	if ( !isset ( $localHulpA[ 'docsize' ] ) ) {  // toevoegen
		$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `docsize` int(16) NOT NULL DEFAULT '0' AFTER `name`", $oFC->file_ref [ 99 ] );
	}	
	if ( !isset ( $localHulpA[ 'keywords' ] ) ) {  // toevoegen
		$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `keywords` varchar(255) NOT NULL DEFAULT '' AFTER `name`", $oFC->file_ref [ 99 ] );
	}
	if ( !isset ( $localHulpA[ 'filetype' ] ) ) {  // toevoegen
		$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `filetype` char(16) NOT NULL DEFAULT '' AFTER `name`", $oFC->file_ref [ 99 ] );
	}
	if ( !isset ( $localHulpA[ 'hash' ] ) ) {  // toevoegen
		$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `hash` char(16) NOT NULL DEFAULT '' AFTER `name`", $oFC->file_ref [ 99 ] );
	}
	if ( !isset ( $localHulpA[ 'date' ] ) ) {  // toevoegen
		$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `date` char(16) NOT NULL DEFAULT '' AFTER `name`", $oFC->file_ref [ 99 ] );
	}
	if ( !isset ( $localHulpA[ 'location' ] ) ) {  // toevoegen
		$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `location` varchar(255) NOT NULL DEFAULT '' AFTER `name`", $oFC->file_ref [ 99 ] );
	}
	if ( !isset ( $localHulpA[ 'area' ] ) ) {  // toevoegen
		$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `area` varchar(64) NOT NULL DEFAULT '' AFTER `name`", $oFC->file_ref [ 99 ] );
	}
	/* achtual upgrade */
	if ( isset ( $job ) && count( $job ) > 0 ) {
		
		foreach( $job as $key => $query ) {
			$oFC->description .= date('G:i:s'.substr ( ( string ) microtime ( ), 1, 8).' : ') . __LINE__  . ' ' . $query . NL;
			$database->simple_query ( $query ); 
		} 
		$oFC->description .= date('G:i:s'.substr ( ( string ) microtime ( ), 1, 8).' : ') . __LINE__  . ' Upgrade function' . NL; 
	}
		/* upgraded */
}

/* Additional functions */
if (LOAD_MODE == "x" && isset ( $xmode ) && strlen ( $xmode ) >3 ) require_once ( $place[ 'includes'] . 'repair.php' );

// display preparation
switch ( $oFC->page_content [ 'MODE' ]  ) {
	case 0:
		break;
	case 8:
		$oFC->page_content [ 'MODE' ] = 9;
	default: // default list
		$pageok = true;
		// bepaal aantal records
		$result = array ( );
		$TEMPLATE = "SELECT count(`id`) FROM `%s` %s";
		$database->execute_query(sprintf ( $TEMPLATE, $oFC->file_ref [ 99 ], $oFC->search_mysql), true, $result);
		$row = current ( $result );
		$oFC->page_content [ 'aantal' ] = $row [ "count(`id`)" ];
		/* paging / accordeon /  make records unique / restore zoek/sort field */
		$limit_sql = $oFC->gsm_pagePosition ("sql", $oFC->page_content [ 'POSITION' ] , $oFC->page_content [ 'aantal' ], $oFC->setting [ 'qty_max' ]  );
		$query  = "SELECT * FROM `" . $oFC->file_ref [ 99 ] . "` ";
		if (strlen($oFC->search_mysql) > 15) {
			$query .= $oFC->search_mysql;
			$query .= " AND `id` > '" . $oFC->recid . "'";
		} else {
			" WHERE `id` > '" . $oFC->recid . "' "; 
		}
		$query .= " ORDER BY `type` ASC, `ref`,  `id` DESC ". $limit_sql;
		/* debug * / Gsm_debug ($query, __LINE__ . __FUNCTION__ ); /* end debug */ 
		$results = array();
		$cal = array();
		$job = array();
		if ( $database->execute_query( $query, true, $results) && count ( $results ) > 0 ) { 
			foreach ( $results as  $row ) {
				$updateArr = array();
				// remove uppercast 
				$localHulp =  $oFC->gsm_sanitizeStrings ( $row [ 'keywords' ], 's{TOASC|LOWER}' ) ; 
				if ( $row [ 'keywords' ] != $localHulp ) $updateArr  [ 'keywords' ] = $localHulp;
				$arout = $oFC->setting [ 'zoek' ] [ 'castor' ];
				// zoek correct ??
				foreach ( $row as $pay => $load ) $arout = str_replace ( "|".$pay."|", "|".$load."|", $arout );
				$arout = str_replace ( "||", "|", $arout );			
				$arout = $oFC->gsm_sanitizeStrings ( $arout, 's{TOASC|LOWER}' ) ; 
				if ( $row [ 'zoek' ] != $arout ) { 
					$row [ 'zoek' ] = $arout; 
					$updateArr ['zoek'] = $row ['zoek']; }
				$row ['link'] = sprintf ("%s%s%s%s",
					$oFC->subdir ?? "",
					$row ['area'], 
					$row ['location'], 
					$row ['name']);
				$cal [] = $row;
				if ( count ( $updateArr ) > 0 ) 
					$job [ ] = "UPDATE `" . $oFC->file_ref [ 99 ] . "` SET " . 
					$oFC->gsm_accessSql( $updateArr, 2 ) . " WHERE `id` = '" . $row [ 'id' ] . "'"; 
			} 
			if ( isset ( $job ) && count ( $job ) > 0 ) {
				foreach ( $job as $key => $query ) $database->simple_query ( $query ) ;
				$oFC->description .= date ( "H:i:s " ) . __LINE__  . " Aantal records aangepast : ". count ( $job ). NL;
			}
		} else {
			$oFC->page_content[ 'STATUS_MESSAGE' ] .= $oFC->language [ 'TXT_ERROR_DATA' ].NL;
		}
		$oFC->page_content [ 'RESULTS' ] = $cal;
		break;
}
/* end display processing */

/* display selection options */
$oFC->page_content [ 'SELECTION' ] = "";
$oFC->page_content [ 'SELECTIONA' ] = "";
$oFC->page_content [ 'SELECTIONB' ] = "";
switch ( $oFC->page_content [ 'MODE' ] ) {
	case 0:
		break;
	default: 
		$oFC->page_content [ 'SELECTIONA' ] = $oFC->gsm_opmaakSel ( array ( 10 ) );
		if ( $oFC->page_content [ 'aantal' ] > $oFC->setting [ 'qty_max' ] ) 
			$oFC->page_content [ 'SELECTIONB' ] = $oFC->gsm_opmaakSel ( array ( 13), "-", "-", $oFC->page_content [ 'POSITION' ], $oFC->page_content [ 'aantal' ] , $oFC->setting [ 'qty_max' ]  );
		break;
} 
 
/* output processing */
/* memory save * /
$oFC->page_content ['MEMORY'] = $oFC->gsm_memorySaved ( ); 

/* als er boodschappen zijn deze tonen in een error blok */
$oFC->page_content[ 'STATUS_MESSAGE' ] .= $oFC->description; 
if (strlen($oFC->page_content[ 'STATUS_MESSAGE' ])>4 ) $oFC->page_content['MESSAGE_CLASS']= "ui error message"; 
$oFC->page_content[ 'VERSIE' ] = $oFC->version; 
if (LOAD_MODE == "x" )  $_SESSION[ 'last_edit_section' ] = $section_id; 

switch ( $oFC->page_content [ 'MODE' ] ) {
	default: 
//		$template_name = '@'.LOAD_MODULE.LOAD_SUFFIX.'/back.lte';
		break;
}

/* actual output */
echo $oTWIG->render( 
	$template_name, // template-filename
    $oFC->page_content // template-data
);

if ($oFC->setting [ 'debug' ] == "yes" ){
	Gsm_debug ($oFC->page_content, __LINE__ . $template_name );  
	if (LOAD_MODE == "x" )  Gsm_debug ($oFC->version, $template_name );
}
?>