# gsmoffa  / castor
 Lepton document archivig function as part of the gsmoff set of applications.

This document is relevant for `Gsmoffa` / `Castor`  version 7.0.0 and up.

## Download
The released stable `gsmoffa` / `Castor` application is recommended to install/update to the latest available version listed. Older versions may contain bugs, lack newer functions or have security issues. 

## License
`gsmoffa` / `castor` is licensed under the [GNU General Public License (GPL) v3.0](http://www.gnu.org/licenses/gpl-3.0.html). The module is available "as is"

### You are free to:
Share — copy and redistribute the material in any medium or format
Adapt — remix, transform, and build upon the material
for any purpose, even commercially. 

### Under the following terms:
Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
No additional restrictions — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.

### Notices:
You do not have to comply with the license for elements of the material in the public domain or where your use is permitted by an applicable exception or limitation.
No warranties are given. The license may not give you all of the permissions necessary for your intended use. For example, other rights such as publicity, privacy, or moral rights may limit how you use the material.

## Pre-conditions
The minimum requirements to get this application running on your LEPTON installation are as follows:

- LEPTON *** 7.0 *** of higher depending on the version
- the module uses Twig support
- the module uses Fomantic
- the taxonomy module installed.

The module is tested in combination with the Office Tegel template

## Installation

This description assumes you have a standard installation at the at least at level Lepton 7.0. All tests are done exclusively with the TFA during installation selected off (not ticked) *)  

1. download the installation package
2. install the downloaded zip archive via the LEPTON module installer
3. create a page which uses the indicated module
4. start the backend functions setupa/instellingen and optionally activate one of the following functions where needed 

  * REMOVE 	remove data (ref = weg or keyword = recycle).
  * IMAGE 	image directory copied from frontend
  * LOGGING 	empty the logging
  * DETAIL 	detailed data
  * INSTALL 	frontend files are created: customized values are overwritten bij default values !!
or enter ? also for not (yet) documented functions

The function can be started by selecting d_...._ where ... is the function name. A number of the function names can be concatenated.

The system will automatically install the file upon the first use of this module. 
 
### the backend functions

- setupa / installation	also called instellingen to setup and manitenace of the data
- castor	the default function to search for documents

the other functions are to be used as administration functions
 
 - edit	  changing the keywords and project info for the entries
 - load	  transfer data from the media/archive directory to the database
 
  Depending on the contents of the SET file modules are made available. It may be needed to modify it to get the required functionality
  
### frontend menu  SET_menu


$FC_SET [ 'SET_function' ] 	= 'setupa'; 	// backend menu ';
$FC_SET [ 'SET_menu' ] 		 = 'castor'; 	// frontend menu ';

// for the administrator and the editor
if ( isset ($_SESSION [ 'GROUPS_ID' ] ) && ( $_SESSION [ 'GROUPS_ID' ] == 1 || 	$_SESSION [ 'GROUPS_ID' ] == 4 ) )  {
	$FC_SET [ 'SET_menu' ] 		= 'castor|edit|load'; 
	$FC_SET [ 'SET_function' ] 	= 'setupa|castor|edit|load';
}

// on the screen there may appear a module name to select.
$FC_SET [ 'SET_txt_menu' ] [ 'xsetupa' ]	= 'Instellingen';
$FC_SET [ 'SET_txt_menu' ] [ 'xdummy' ]		= '-dummy-';
$FC_SET [ 'SET_txt_menu' ] [ 'vdummy' ]		= '-dummy-';
$FC_SET [ 'SET_txt_menu' ] [ 'vcastor' ]	= 'Archief';
$FC_SET [ 'SET_txt_menu' ] [ 'vedit' ]		= 'Aanpassen';
$FC_SET [ 'SET_txt_menu' ] [ 'vload' ]		= 'Toevoegen';
$FC_SET [ 'SET_txt_menu' ] [ 'xcastor' ]	= 'Archief';
$FC_SET [ 'SET_txt_menu' ] [ 'xedit' ]		= 'Aanpassen';
$FC_SET [ 'SET_txt_menu' ] [ 'xload' ]		= 'Toevoegen';
	 
### droplets related to functionality of this module


#### The droplet Gsm_mediadocIn

droplet to upload files to system (to default the intermediate directory: media/archive directory)
Usage [[Gsm_mediadocIn?mediadir=/archive&project=XX&login=no]] 
 - mediadir=/archive 	to change the media subdirectory
 - project=XX 			to change the project prefix
 - login=no 			yes will allow only logged in persons to access the function
 
e.g [[Gsm_mediadocIn]]

note this droplet_Gsm_mediadoc is installed with the Taxonomy module

#### The droplet Gsm_mediadoc

droplet to give access to the media in a certain directory
Usage [[Gsm_mediadoc?mediadir=/archive&project=XX&width=&message=200&login=no&seq=s]] 

 - mediadir=/archive 	to change the media subdirectory
 - project=XX 			add another subdirectory
 - width=200			display width (for jpg only
 - message=No data		message when the directory is empty
 - login=no				yes will allow only logged in persons to access the data
 - seq=s				display mode seq=r is reversed sequence. card gives different layout methods

e.g. [[Gsm_mediadoc?mediadir=/assets/layout/castordoc]] 

e.g. [[Gsm_mediadoc?project=XX1]]  	so default directory and subdirectory XX1

e.g. [[Gsm_mediadoc?seq=card]] 		different layout

note this droplet_Gsm_mediadoc is installed with the Taxonomy module

#### The droplet Gsm_castordoc

displays the content op specific area's of the document archive
Usage [[Gsm_castordoc?project=XXdroplet&width=&message=&login=&seq=]] 

 - project=XXdroplet 	selects the indicated prefix (type == XX) and optionally the indicated reference (ref = droplet
 - width=200			display width (for jpg only
 - message=No data		message when the directory is empty
 - login=no				yes will allow only logged in persons to access the data
 - seq=s				display mode seq=r is reversed sequence. card gives different layout methods

 e.g. [[Gsm_castordoc]]

 e.g. [[Gsm_castordoc?project=XXdroplet&seq=card&login=yes&message=login first]]

## error messages
 * When started for the first time an error message :*Oeps system not initialised and/or empty database* can be seen.
 The database tables are to be initialised:-> backend -> setupa
 The menusystem is installed: -> backend -> setup with parameter d_install detailed_