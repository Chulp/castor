# gsmoffa  / castor
 Lepton document archivig function as part of the gsmoff set of applications.

This document is relevant for `Gsmoffa` / `Castor`  version 7.0.0 and up.

## Download
The released stable `gsmoffa` / `Castor` application is recommended to install/update to the latest available version listed. Older versions may contain bugs, lack newer functions or have security issues. 

## License
`gsmoffa` / `castor` is licensed under the [GNU General Public License (GPL) v3.0](http://www.gnu.org/licenses/gpl-3.0.html). The module is available "as is"

## setup file set.php

The set file contains

initially
	$FC_SET [ 'SET_menu' ] 		= 'dummy'; 
	$FC_SET [ 'SET_function' ] 	= 'setupa|dummy';
So basically only the backend setup function is allowed.

After setup it could look like

	$FC_SET [ 'SET_menu' ] 		= 'castor'; 	// access to stored documents in the front end
	$FC_SET [ 'SET_function' ] 	= 'setupa|castor|edit|load'; // access, stor and alter functions in the backend 

It is possible to add functions in the front as well e.g. with access to the administrator only 
	$FC_SET [ 'SET_menu' ] 		= 'castor|edit|load'; 			// access to stored documents in the front end
	$FC_SET [ 'SET_function' ] 	= 'setupa|castor|edit|load'; 	// access, stor and alter functions in the backend 

### For taxonomy (gsmofft) data

the following is suggested

	 1. setting	allowed|gsmoffa 	pdf|jpg|mp4|html|zip 	(default)
	 2. setting	allowed|gsmoffa|1 	pdf|jpg|mp4|html|zip|png|txt|odt|doc|csv|sql (optional)
	 2. setting	allowed|gsmoffa|2 	pdf|jpg|mp4|html|zip|png|txt|odt|ods|doc|docx|xls|xlsx|ppt|pptx|csv|sql
	 3. setting entity|gsmoffa 		XX|PV|CH|MM|SW|OM|AB 	(optional)
	 
	 4. setting mediadir|gsmofft	media/archive	(default)
	 5. setting collectdir|gsmofft	media/logging	(default)
	 6. setting datadir|gsmofft		/castor			(default)
	 7. setting owner				XX 	
	 8. setting qty_max				60 	


### The droplet Gsm_castordoc

droplet to give access to selected media in a the document archive
Usage [[Gsm_castordoc?project=XX&width=200&message=No data&login=yes&seq=s]]  
n.b. if project is blank login=yes 

 - project=XXabc		the castor project ref 
 - width=200 			display width ( for jpg )
 - message=No data		message when the project is empty
 - login=no				yes will allow only logged in persons to access the data
 - seq=s				display mode seq=r is reversed sequence. card gives different layout methods

e.g. [[Gsm_castordoc?project=XX01]] 

e.g. [[Gsm_castordoc?project=XX01&seq=card]]  	so default directory and subdirectory XX1	  
	  
#### error messages
 * When started for the first time an error message :*Oeps system not initialised and/or empty database* can be seen. The database tables are to be initialised:-> backend  -> setup
