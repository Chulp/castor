<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2024 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php

$MOD_GSMOFFA = array(
	'OWN' => "MOD_GSMOFFA",
	'LANG' => "NL",
	'VERS' => "v20240430",
		
	'active' => array ( 
		'0' => 'niet actief', 
		'1' => 'actief'),
		
	'DUMMY' => array (
		'0' => 'Geen functionaliteit. Database niet geinitialiseerd, geen rechten of foute instellingen',
		'1' => 'Dummy module zonder functionaliteit is gestart',
		'2' => 'Controleer of de initiele routines zijn uitgevoerd'),
		
	'tbl_icon' => array ( 
		1 =>'View', 
		2 =>'Return', 
		3 =>'Add',
		4 =>'Save',  
		5 =>'Save (as new)', 
//		6 =>'Remove', 
//		7 =>'Calculate',
//		8 =>'Check',
		9 =>'Select', 
//		10 =>'+',
//		11 =>'Print', 
//		12 =>'Set',
//		13 =>'reserved',
//		14 =>'Next',
//		15 =>'Test',
//		16 =>'Mail',
		17 =>'Process', 
		18 =>'Invoicing', 
//		19 =>'Balans', 
//		20 =>'Result' ,
		21 =>'Verwerk'
	), 

	'TXT_ACTIVE_DATA'	=> ' Actief record gevonden' ,	
	'TXT_CONSISTENCY'	=> ' Oeps consistency controle',
	'TXT_DATABASE_NEW'	=> ' Initial record added ',
	'TXT_DIR_CREATION' => ' Directory aangemaakt',
	'TXT_ERROR_ADRES'	=> ' Oeps naam en of adres informatie ontbreekt',
	'TXT_ERROR_DATA' 	=> ' Oeps geen data ',  
	'TXT_ERROR_INIT'	=> ' Oeps systeem niet geinitialiseerd en/of lege database ',
	'TXT_ERROR_SIPS'	=> ' Oeps sips actief ',
	'TXT_ERROR_PAGE'	=> ' Oeps unexpected situation ',	
	'TXT_LOGIN' 		=> ' Login ',
	'TXT_LOGIN_ERROR_SHORT' => ' Not a valid e-mail address or password too short. ',
	'TXT_LOGIN_NOW' => ' Uw login data is aangepast. Login met uw nieuwe gegevens. ',
	'TXT_LOGIN_REGISTER' => ' Register / Change Password ',
	'TXT_LOGIN_SETT'	=> ' Correct Login Settings  ',
	'TXT_LOGIN_VERIFY' => ' Verificatie ',
	'TXT_MAINTENANCE' 	=> ' Maintenance ', 	
	'TXT_NO_ACCESS'		=> '(Partner) Access not available ',
	'TXT_REC_CHANGE'	=> ' Aantal records aangepast : ',
	'TXT_REMOVE_REF'	=> 'weg',
	'TXT_REMOVE_KEYWORD'=> 'recycle',
	'TXT_SETUP' 		=> ' Setup ',  

);
?>