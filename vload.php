<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2025 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php
 
// start
$module_name = 'vload';
$version = '20250118';
$project = "Archive Uploaded Documents";
$main_file = "castor";
$default_template = '/load.lte';

/* start initialize module */
global $oLEPTON;
$oFC = gsmoffa::getInstance();
$oTWIG = lib_twig_box::getInstance ( );
$oTWIG-> registerModule ( LOAD_MODULE . LOAD_SUFFIX );
$template_name= '@' . LOAD_MODULE . LOAD_SUFFIX . "/". LANGUAGE . $default_template;

/* version data */
$oFC->version [ $module_name ] = $version;
$oFC->version = array_merge ( $oFC->version, $version_display);
$oFC->version [ $oFC-> language [ 'LANG' ] ] = $oFC-> language [ 'VERS' ];
$oFC->version [ "set" ] = $FC_SET [ 'version' ] ;

/* file references */
$oFC->file_ref  [ 99 ] = LOAD_DBBASE . "_".$main_file;

/* settings */
$oFC->setting [ 'includes' ] 		= $place [ 'includes' ];
$oFC->setting [ 'frontend' ] 		= $place [ 'frontend' ];
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET );
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "zoek" );
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "entity");

/* default values */
$oFC->page_content [ 'FORMULIER' ] = $project;
$oFC->page_content [ 'MODULE' ] = $module_name;
$oFC->page_content [ 'MODE' ] = 9;
$oFC->page_content [ 'PAGE_ID' ] = $page_id ?? 0;
$oFC->page_content [ 'SECTION_ID' ] = $section_id ?? 0;

/* Gebruik limited door niet rechthebbenden */
if ( $oFC->user [ 'privileged' ] > 0 ) {
	$oFC->user  = array_merge (	$oFC->user, $oFC->gsm_adresDet ( $oFC->page_content [ 'PAGE_ID' ], $oFC->setting [ 'owner' ] ) );
	if ($oFC->user[ 'privileged' ] > 2 ) {
		$oFC->page_content [ 'MODE' ] = 9;
		$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET );
	}
	/* niet genoeg rechten */
	if ( $oFC->page_content [ 'MODE' ] == 0 ) {
		$oFC->description = $oFC->language [ 'TXT_NO_ACCESS' ];
		unset ( $_POST[ 'command' ] );
	}
	/* end genoeg rechten */
}

/* create condition for sips test */
$_SESSION[ 'page_h' ] = $oFC->page_content ['HASH'];

/* get memory values */ 
$oFC->gsm_memorySaved ( );

if ($oFC->setting [ 'debug' ] == "yes" ) Gsm_debug (array ("post"=> $_POST, "get"=>$_GET ?? "", "this"=>$oFC, $selection ?? ""), __LINE__ . __FUNCTION__ ); 

/* Input processing */
if ( isset( $_POST[ 'command' ] ) ) {
	switch ( $_POST[ 'command' ] ) {
		case "up":
			$oFC->page_content [ 'POSITION' ] = $oFC->gsm_pagePosition ("up", $oFC->page_content [ 'POSITION' ] , $_POST[ 'n2' ],  $oFC->setting [ 'qty_max' ], $_POST[ 'n0' ], $_POST[ 'n1' ] );
			break;
		case "down":
			$oFC->page_content [ 'POSITION' ] = $oFC->gsm_pagePosition ("down", $oFC->page_content [ 'POSITION' ] , $_POST[ 'n2' ], $oFC->setting [ 'qty_max' ], $_POST[ 'n0' ], $_POST[ 'n1' ] );
			break;
		case "View":
			break;
		case "Reset":
			break;
		case "Proces":			
		case "Save":
			$PostArr = array();
			$DATfilter  = sprintf ("y{%s;%s;%s}", 
				date( "Y-m-d", time() ), 
				date( "Y", time() )-100 . "-01-01", 
				date( "Y", time() ) +2  . "-12-31"); 
			foreach ($_POST as $key => $value ) { 
				$localHulpA = explode ("|", $key);
				if ( isset ( $localHulpA [0] ) && 
					$localHulpA [ 0 ] == "v" ) { 
					$PostArr [ $localHulpA [ 1 ] ] [ 'del' ] = $value;
				}
				if ( isset ( $localHulpA [ 0 ] ) && 
					$localHulpA [ 0 ] == "x" ) { 
					$PostArr [ $localHulpA [ 1 ] ] [ 'oke' ] = $value;
				}
				if ( isset ( $localHulpA [0] ) && 
					$localHulpA [ 0 ] == "a" ) { 
					$PostArr [ $localHulpA [ 1 ] ] [ 'date' ] = $oFC->gsm_sanitizeStringD ( $value, $DATfilter );
				}
				if ( isset ( $localHulpA [ 0 ] ) && 
					$localHulpA [ 0 ] == "b" ) { 
					$PostArr [ $localHulpA [ 1 ] ] [ 'type' ] = $oFC->gsm_sanitizeStringS ( $value,
					"s{STRIP|TOASC|CLEAN}");
				}
				if ( isset ( $localHulpA [ 0 ] ) && 
					$localHulpA [ 0 ] == "c" ) { 
					$PostArr [ $localHulpA [ 1 ] ] [ 'ref' ] = $oFC->gsm_sanitizeStringS ( $value,
					"s{STRIP|TOASC|LOWER|CLEAN}" );
				}
				if ( isset ( $localHulpA [ 0 ] ) && 
					$localHulpA [ 0 ] == "d" ) { 
					$PostArr [ $localHulpA [ 1 ] ] [ 'keywords' ] = $oFC->gsm_sanitizeStringS ( $value,
					"s{STRIP|TOASC|LOWER|CLEAN}" );
				}
				if ( isset ( $localHulpA [ 0 ] ) && 
					$localHulpA [ 0 ] == "z" ) { 
					$PostArr [ $localHulpA [ 1 ] ] [ 'content_short' ] = $value;
				}
			}
			/* debug * / gsm_debug ( array($PostArr , $_POST [ 'para_pr'], $_POST [ 'para_pf']), __LINE__ . __FUNCTION__ ); /* end debug */ 
			foreach ($PostArr as $result) {
				if (isset ( $_POST [ 'para_pf'] ) && strlen ( $_POST [ 'para_pf' ] ) >1 ) $result [ "type" ] = strtoupper ( $_POST [ 'para_pf' ] );
				if (isset ( $_POST [ 'para_pr'] ) && strlen ( $_POST [ 'para_pr' ] ) >1 ) $result [ "ref" ] = $oFC->gsm_sanitizeStringS( $_POST [ 'para_pr' ], "s{STRIP|TOASC|LOWER|CLEAN}" );
				if ( isset ( $result [ 'oke' ] ) ) {
					$insertArr = array ();
					$insertArr [ 'type' ]  = $result [ "type" ];
					$insertArr [ 'ref' ] = $result [ "ref" ];
					$insertArr [ 'date' ] = $result [ "date" ];
					$insertArr [ 'hash' ] = strtolower( substr( $oFC->gsm_guid(), 0, 6 ) );
					$insertArr [ 'keywords' ] = strtolower ( $result [ "keywords" ] );
					$insertArr [ 'filetype' ] = trim(strtolower( substr( $result [ 'content_short' ], strrpos( $result [ 'content_short' ], '.' ) + 1 ) ));
					$insertArr [ 'location' ] = sprintf ( "/%s/%s/", $result [ "type" ], substr ($insertArr [ 'hash' ], 0, 2 ) );
					$insertArr [ 'area' ] = $oFC->setting [ 'datadir' ];
					$insertArr [ 'content_short' ] = str_replace ( $oFC->setting [ 'mediadir' ], "", str_replace ( LEPTON_PATH, "", $result [ "content_short" ] ) );
					$insertArr [ 'name' ] =sprintf ( "%s%s_%s%s_%s.%s" ,
						$insertArr [ 'type' ],
						$insertArr [ 'ref' ],
						$insertArr [ 'date' ],
						$insertArr [ 'hash' ],
						str_replace ( " ", "_", $insertArr [ 'keywords' ]),
						$insertArr [ 'filetype' ] );
					$temp = $oFC->setting [ 'zoek' ] [ 'castor' ];	
					foreach ( $insertArr as $pay => $load ) $temp = str_replace ( "|".$pay."|", "|".$load."|", $temp );
					$temp = str_replace ( "||", "|", $temp );
					$temp = $oFC->gsm_sanitizeStrings ( $temp, 's{TOASC|LOWER}' ); 
					$insertArr [ 'zoek' ] = $temp;
					$location = $result [ 'content_short' ];
					$location_new = LEPTON_PATH. $insertArr['area']. $insertArr['location']. $insertArr['name'];
					$dir_to = LEPTON_PATH . $insertArr [ 'area' ] . "/" . $insertArr [ 'type' ] . "/" . substr ( $insertArr [ 'hash' ], 0, 2 );
					if (count ($insertArr) > 5 &&  file_exists ( $location ) ) {
						$oFC->gsm_existDir ( $dir_to, true);
						if ( rename ($location, $location_new) ) {
							$database->build_and_execute(
								"insert",
								$oFC->file_ref[ 99 ],
								$insertArr);
							$oFC->description .= sprintf ( 'loaded into the database  : %s as<br/>%s' , $insertArr [ "content_short" ], $insertArr [ "name" ] ) . NL . NL;
						}
					}
				} elseif ( isset ( $result [ 'del' ] ) ) {
					if ( unlink ( $result [ 'content_short' ] ) ) $oFC->description .= sprintf ( 'removed : %s' , str_replace ( LEPTON_PATH, "", $result [ "content_short" ] ) ) ;
				}
			}
			break;
		case "Select":
		default:
			break;
	} 
} elseif ( isset( $_GET[ 'command' ] ) ) {
	switch ( $_GET[ 'command' ] ) {
		case 'select':  
		default:
			// escape route 
			break;
	} 
} else {
	// so standard display / first run
}

if ($oFC->page_content [ 'MODE' ] == 0) $selection .= " pf:".substr( $oFC->user[ 'ref' ], 0, 2 );
if ( isset ( $selection ) ) {
	$selection = $oFC->gsm_ParameterEval ( $selection , "castor" );
	$oFC->page_content [ 'PARAMETER' ] = $selection;
	$oFC->page_content [ 'SUB_HEADER' ] = strtoupper ( $oFC->page_content[ 'PARAMETER' ] );
	$oFC->search_mysql = $oFC->gsm_ParameterSearch (  $selection, "castor" );
	/* debug * / gsm_debug ($oFC->search_mysql, __LINE__ . __FUNCTION__ );  /* end debug */ 
}

// display preparation

switch ( $oFC->page_content [ 'MODE' ] ) {
	case 0:
		$oFC->setting [ 'entity' ]  = array ( $oFC->setting [ 'owner' ] ) ;  // beperkte toegang
		break;
	default:
		$results = $oFC->gsm_scanDir ( LEPTON_PATH. $oFC->setting [ 'mediadir' ],  2, $oFC->setting [ 'qty_max' ], 'all', 'all', '' );
		if ( !is_array ( $oFC->setting [ 'entity' ]  ) ) $oFC->setting [ 'entity' ]  = explode ("|" , $oFC->setting [ 'entity' ]  );
		$cal = array();
		$n=1;
		foreach ( $results as $key => $value ) {
			if ( $key == "index.php" ) continue;			
			$result = array();
			$result [ 'n' ] = $n;
			$result [ 'name' ] = $key;
			$result [ 'keywords' ] = $key;
			$result [ 'content_short' ] = $value;
			$result [ 'filetype' ] = trim(strtolower( substr( $result [ 'keywords' ], strrpos( $result [ 'keywords' ], '.' ) + 1 ) ));
			$result [ 'keywords' ] = str_replace ( ".".$result [ 'filetype' ], "", $result [ 'keywords' ]);
			$result [ 'keywords' ] = str_replace ( array("_", "-"), " ", $result [ 'keywords' ] );
			$temp = explode ( " ", $result [ 'keywords' ] );
			$result [ 'item' ] = $temp;
			// is er een item met 4 cyfers tusssen 1000 en 2200
			$first = true;
			foreach( $temp as $pay => $load) {
				if (!isset ($result [ 'date' ] ) && is_numeric ( $load ) && $load > 1000 && $load < 2200 ) { // jaar gevonden) 
					$tempA = $load; 
					$result [ 'keywords' ] = str_replace ( $tempA, "", $result [ 'keywords' ]);
					$tempB = "01" ; 
					$tempC = "01"; 
					$tempK = $pay +1;
					if ( isset( $temp [ $tempK ] ) && is_numeric ( $temp [ $tempK ] ) && 
						$temp [ $tempK ] > 0 && $temp [ $tempK ] < 13) { 
							$tempB = $temp [ $tempK ]; 
							$tempK++; 
							$result [ 'keywords' ] = str_replace ( "".$tempB." ", " ", $result [ 'keywords' ] );
						}
					if ( isset( $temp [ $tempK ] ) && is_numeric ( $temp [ $tempK ] ) && 
						$temp [ $tempK ] > 0 && $temp [ $tempK ] < 32) { 
							$tempC = $temp [ $tempK ];
							$result [ 'keywords' ] = str_replace ( "".$tempC." ", " ", $result [ 'keywords' ] );
					} elseif ( isset( $temp [ $tempK ] )) {
						$tempE = substr ($temp [ $tempK ], 0 , 2 ); 
						if ( is_numeric ( $tempE ) && 
							$tempE > 0 && $tempE < 32) { 
								$tempC = $tempE;
								$result [ 'keywords' ] = str_replace ( $temp [ $tempK ], " ", $result [ 'keywords' ] ); 
						}
					}
					$result [ 'date' ] = sprintf ( '%s-%s-%s', $tempA,$tempB,$tempC);
				}  else {
					if ($first) {
						$temp1 = substr ( $load, 0 , 2 ); 
						$temp2 = substr ( $load, 2 ); 
						if (in_array( $temp1, $oFC->setting [ 'entity' ]  ) ) {
							$result ['type'] = strtoupper ( $temp1 );
							$result ['ref'] = strtolower ( $temp2 );
							$result [ 'keywords' ] = str_replace ( $load , " ", $result [ 'keywords' ] ) ;
						} else { 
							$result ['type'] =strtoupper ($oFC->setting [ 'owner' ]);
							$result ['ref'] = '';
						}
						if (strlen ($oFC->memory [5] ) == 2) $result ['type'] = $oFC->memory [5];
						if (strlen ($oFC->memory [4] ) > 1) $result ['ref'] = $oFC->memory [4];
						$first=false;
					}
				}
			}
			$ssf = stat( $value );
			$result [ 'docsize' ] = $ssf[ 'size' ];
			if (!isset ($result [ 'date' ] ) ) $result [ 'date' ] = date( "Y-m-d", time() );
			$temp = explode ( " ", $result [ 'keywords' ] );
			$result [ 'keywords' ] = "";
			foreach( $temp as $load) {if (strlen ($load) > 1) $result [ 'keywords' ] .= $load." ";}
			unset ( $result [ 'item' ] );
			$result [ 'GSM_TYPE' ] = $oFC->gsm_selectOption ( $oFC->setting [ 'entity' ] , $result [ 'type' ] ?? " ", 2);
			$result [ 'keywords' ] = trim($result [ 'keywords' ]);
			$result [ 'name' ] = sprintf ( '%s%s %s %s.%s', $result [ 'type' ] ?? "", $result [ 'ref' ] ?? "--", $result [ 'date' ], $result ['keywords'], $result ['filetype']);
			switch ( $result [ 'filetype' ] ) {
				case "jpg":
					$result [ 'toon' ] = sprintf ( '<a href="%1$s"><img src="%1$s%2$s" width="500" height=" "/></a>', $oFC->subdir ?? "",str_replace ( LEPTON_PATH, LEPTON_URL, $result [ "content_short" ] ) );
					break;
				case "pdf":
					$result [ 'toon' ] = sprintf ('<center><embed height="400" width="580" src="%1$s%2$s#toolbar=1&navpanes=0&scrollbar=1"></embed></center>', $oFC->subdir ?? "", str_replace ( LEPTON_PATH, LEPTON_URL , $result [ "content_short" ] ) );
					break;
				default:
					$result [ 'toon' ] = sprintf ( '<a href="%1$s%2$s" target="_apart">%2$s</a>', $oFC->subdir ?? "", str_replace ( LEPTON_PATH, LEPTON_URL , $result [ "content_short" ] ) );
					break;
			}
			$cal [ ] = $result;
			$n++;
		}
		$oFC->page_content [ 'RESULTS' ] = $cal;
		break;	
}

/*
 * the selection options
 */
 
switch ( $oFC->page_content [ 'MODE' ] ) {
	case 0:
		break;
	default: 
		if ( !is_array ( $oFC->setting [ 'entity' ] ) ) $oFC->setting [ 'entity' ] = explode ("|" , $oFC->setting [ 'entity' ] );
		if ( is_array ( $oFC->setting [ 'entity' ] ) ) array_unshift ( $oFC->setting [ 'entity' ], " " );
		$oFC->page_content[ 'GSM_TYPE' ] = $oFC->gsm_selectOption ( $oFC->setting [ 'entity' ], ' ', 2);
		array_unshift ( $oFC->setting [ 'allowed' ], " " );
		$oFC->page_content[ 'GSM_FILETYPE' ] = $oFC->gsm_selectOption ( $oFC->setting [ 'allowed' ], '-', 3);
		$oFC->page_content [ 'SELECTIONE' ] = $oFC->gsm_opmaakSel ( array ( 7 ) );
		$oFC->page_content [ 'SELECTIONC' ] ="";
		break;
}

/* output processing */
/* memory save */
$oFC->page_content ['MEMORY'] = $oFC->gsm_memorySaved ( 2 ); 

/* als er boodschappen zijn deze tonen in een error blok */
$oFC->page_content[ 'STATUS_MESSAGE' ] .= $oFC->description; 
if (strlen($oFC->page_content[ 'STATUS_MESSAGE' ])>4 ) $oFC->page_content['MESSAGE_CLASS']= "ui error message"; 
$oFC->page_content[ 'VERSIE' ] = $oFC->version; 
$oFC->page_content [ 'MODE' ] = $oFC->page_content [ 'MODE' ];
if (LOAD_MODE == "x" )  $_SESSION[ 'last_edit_section' ] = $section_id; 

switch ( $oFC->page_content [ 'MODE' ] ) {
	default: 
		break;
}

/* actual output */
echo $oTWIG->render( 
	$template_name, // template-filename
    $oFC->page_content // template-data
);

if ($oFC->setting [ 'debug' ] == "yes" ){
	Gsm_debug ($oFC->page_content, __LINE__ . $template_name );  
	if (LOAD_MODE == "x" )  Gsm_debug ($oFC->version, $template_name );
}
?>