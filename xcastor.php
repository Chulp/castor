<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2025 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php
 
/* module id */  
$module_name = 'xcastor';
$version='20250118';
$project = "Document Archief";
$main_file = "castor";
$default_template = '/castor.lte';

/* start initialize module */
global $oLEPTON;
$oFC = gsmoffa::getInstance();
$oTWIG = lib_twig_box::getInstance ( );
$oTWIG-> registerModule ( LOAD_MODULE . LOAD_SUFFIX );
$template_name= '@' . LOAD_MODULE . LOAD_SUFFIX . "/". LANGUAGE . $default_template;

/* version data */
$oFC->version [ $module_name ] = $version;
$oFC->version = array_merge ( $oFC->version, $version_display);
$oFC->version [ $oFC-> language [ 'LANG' ] ] = $oFC-> language [ 'VERS' ];
$oFC->version [ "set" ] = $FC_SET [ 'version' ] ;

/* file references */
$oFC->file_ref  [ 99 ] = LOAD_DBBASE . "_".$main_file;

/* settings */
$oFC->setting [ 'includes' ] 		= $place [ 'includes' ];
$oFC->setting [ 'frontend' ] 		= $place [ 'frontend' ];
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET );
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "zoek" );
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "entity");

/* default values */
$oFC->page_content [ 'FORMULIER' ] = $project;
$oFC->page_content [ 'MODULE' ] = $module_name;
$oFC->page_content [ 'MODE' ] = 9;
$oFC->page_content [ 'PAGE_ID' ] = $page_id ?? 0;
$oFC->page_content [ 'SECTION_ID' ] = $section_id ?? 0;

/* Gebruik limited door rechthebbenden */
if ( $oFC->user [ 'privileged' ] > 0 ) {
	$oFC->user  = array_merge (	$oFC->user, $oFC->gsm_adresDet ( $oFC->page_content [ 'PAGE_ID' ], $oFC->setting [ 'owner' ] ) );
	if ($oFC->user[ 'privileged' ] > 2 ) {
		$oFC->page_content [ 'MODE' ] = 9;
		$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET );
	}
	/* niet genoeg rechten */
} else {	
	$oFC->page_content [ 'MODE' ] = 0;
	/* end genoeg rechten */
}

/* create condition for sips test */
$_SESSION[ 'page_h' ] = $oFC->page_content ['HASH'];

/* get memory values */
$oFC->gsm_memorySaved ( );

if ($oFC->setting [ 'debug' ] == "yes" ) Gsm_debug (array ("post"=> $_POST, "get"=>$_GET ?? "", "this"=>$oFC, $selection ?? ""), __LINE__ . __FUNCTION__ ); 

/* Input processing */
if ( isset( $_POST[ 'command' ] ) ) {
	switch ( $_POST[ 'command' ] ) {
		case "up":
			$oFC->page_content [ 'POSITION' ] = $oFC->gsm_pagePosition ("up", $oFC->page_content [ 'POSITION' ] , $_POST[ 'n2' ],  $oFC->setting [ 'qty_max' ], $_POST[ 'n0' ], $_POST[ 'n1' ] );
			break;
		case "down":
			$oFC->page_content [ 'POSITION' ] = $oFC->gsm_pagePosition ("down", $oFC->page_content [ 'POSITION' ] , $_POST[ 'n2' ], $oFC->setting [ 'qty_max' ], $_POST[ 'n0' ], $_POST[ 'n1' ] );
			break;
		case "View":
			break;
		case "Reset":
			$oFC->recid = '';
			unset ( $_POST [ 'zoek_pf' ] );
			unset ( $_POST [ 'zoek_pr' ] );
            unset ( $_POST [ 'zoek_bd' ] );
            unset ( $_POST [ 'zoek_sn' ] );
            unset ( $_POST [ 'zoek_ft' ] );
			$selection= "";
			$oFC->page_content [ 'PARAMETER' ] = $selection;
			$oFC->selection = $selection;
			$oFC->page_content [ 'SUB_HEADER' ]= "____";
			break;
		case "Select":
		default:
			// escape route 
			$oFC->page_content [ 'MODE' ] = 9;
			break;
	} 
} elseif ( isset ( $_GET [ 'command' ] ) ) {
	switch ( $_GET[ 'command' ] ) {
		case 'select': //    1=>'Wijzigen', 
			// is a record selected ? 
			if ( !isset( $_GET[ 'recid' ] ) ) {
				$oFC->page_content [ 'MODE' ] = 9;
				$oFC->description .= $oFC->language ['TXT_ERROR_DATA'];
				break;
			} 
			// yes	  
			$oFC->recid = $_GET[ 'recid' ];
			$oFC->page_content [ 'MODE' ] = 8;
			break;
		default:
			// escape route 
			$oFC->page_content [ 'MODE' ] = 9;
			break;
	} 
} else {
	// so standard display / first run
//	$oFC->page_content [ 'MODE' ] = 0;
}

if ( $oFC->page_content [ 'MODE' ] == 0 ) $selection .= " pf:" . substr ( $oFC->setting [ 'owner' ] , 0, 2 );
if ( isset ( $selection ) ) {
	$selection = $oFC->gsm_ParameterEval ( $selection , "castor" );
	$oFC->page_content [ 'PARAMETER' ] = $selection;
	$oFC->page_content [ 'SUB_HEADER' ] = strtoupper ( $oFC->page_content[ 'PARAMETER' ] );
	$oFC->search_mysql = $oFC->gsm_ParameterSearch ( $selection, "castor" );
}

// display preparation
switch ( $oFC->page_content [ 'MODE' ] ) {
	case 0:
		$oFC->page_content [ 'MODE' ] = 9;
	default: // default list
		$pageok = true;
		// bepaal aantal records
		$result = array ( );
		$query = "SELECT count(`id`) FROM `" . $oFC->file_ref [ 99 ] . "` ";
		$query .= $oFC->search_mysql;
		$database->execute_query( $query, true, $result );
		$row = current ( $result );
		$oFC->page_content [ 'aantal' ] = $row [ "count(`id`)" ];
		
		/* paging / accordeon /  make records unique / restore zoek/sort field */
		$limit_sql = $oFC->gsm_pagePosition ("sql", $oFC->page_content [ 'POSITION' ] , $oFC->page_content [ 'aantal' ], $oFC->setting [ 'qty_max' ]  );
		$query  = "SELECT * FROM `" . $oFC->file_ref [ 99 ] . "` ";
		$query .= $oFC->search_mysql;
		$query .= " ORDER BY `type` ASC, `ref`,  `id` DESC ". $limit_sql; 
		$results = array();
		$cal = array();
		$job = array();
		$database->execute_query( $query, true, $results);
		if ( count ( $results ) > 0 ) { 
			$PL0 = "=="; //break controle veld
			foreach ( $results as  $row ) {
				// telt er iets wel mee
				if ( in_array ( $row [ 'filetype' ], $oFC->setting [ 'allowed' ] )  &&
					isset ( $oFC->setting [ 'entity' ] [ $row [ 'type' ] ] ) )  { 
					$groep_op_velden = $row [ 'type' ]. " " . $row [ 'ref' ];
					if ( $PL0 != $groep_op_velden ) {
						if (count ( $cal ) > 0 ) {
							$displayOk = false;
							// conditie
							$displayOk = true;
							if ( $displayOk ) $oFC->page_content [ 'RESULTS' ][ $PL0 ] = $cal;
							$cal = array();
					}	}
					$PL0 = $groep_op_velden;
					// einde met groepering
					$row ['link'] = sprintf ("%s%s%s%s",
						$oFC->subdir ?? "",
						$row ['area'], 
						$row ['location'], 
						$row ['name']);
					$cal []= $row;
				} else {
					// dit telt blijkbaar niet mee
				}
				if ( count ( $cal ) > 0 ) { // de laatste
					$displayOk = false;
					// conditie
					$displayOk = true;
					if ( $displayOk ) $oFC->page_content [ 'RESULTS' ][ $PL0 ] = $cal;
			}	}		
		} else {
			$oFC->page_content[ 'STATUS_MESSAGE' ] .= $oFC->language [ 'TXT_ERROR_DATA' ].NL;
			if ( strlen( $oFC->search_mysql ) < 10 ) 
				$oFC->page_content[ 'STATUS_MESSAGE' ] .= $oFC->language [ 'TXT_ERROR_INIT' ].NL;
		}
		break;
}

/* display selection options */
switch ( $oFC->page_content [ 'MODE' ] ) {
	case 0:
		break;
	default: 
		if ( !is_array ( $oFC->setting [ 'entity' ] ) ) $oFC->setting [ 'entity' ] = explode ("|" , $oFC->setting [ 'entity' ] );
		if ( is_array ( $oFC->setting [ 'entity' ] ) ) array_unshift ( $oFC->setting [ 'entity' ], " " );
		$oFC->page_content [ 'GSM_TYPE' ] = $oFC->gsm_selectOption ( $oFC->setting [ 'entity' ] , $result [ 'type' ] ?? " ", 2);
		array_unshift ( $oFC->setting [ 'allowed' ] , " " );
		$oFC->page_content[ 'GSM_FILETYPE' ] = $oFC->gsm_selectOption (  $oFC->setting [ 'allowed' ], '-', 3);
		
		$oFC->page_content [ 'SELECTIONC' ] = $oFC->gsm_opmaakSel ( array ( 1, 6 ) );
		if ( $oFC->page_content [ 'aantal' ] > $oFC->setting [ 'qty_max' ] ) 
			$oFC->page_content [ 'SELECTIONB' ] = $oFC->gsm_opmaakSel ( array ( 13), "-", "-", $oFC->page_content [ 'POSITION' ], $oFC->page_content [ 'aantal' ] , $oFC->setting [ 'qty_max' ]  );
		break;
}
 
/* output processing */

/* memory save */
$oFC->page_content ['MEMORY'] = $oFC->gsm_memorySaved ( 2 ); 

$oFC->page_content[ 'STATUS_MESSAGE' ] .= $oFC->description; 
if (strlen($oFC->page_content[ 'STATUS_MESSAGE' ])>4 ) $oFC->page_content['MESSAGE_CLASS']= "ui error message"; 
$oFC->page_content[ 'VERSIE' ] = $oFC->version; 
$oFC->page_content [ 'MODE' ] = $oFC->page_content [ 'MODE' ];
if (LOAD_MODE == "x" )  $_SESSION[ 'last_edit_section' ] = $section_id; 
 
if ( isset( $oFC->page_content[ "GSM_ID" ] ) ) $oFC->page_content[ "RECID" ] = $oFC->page_content[ "GSM_ID" ];

switch ( $oFC->page_content [ 'MODE' ] ) {
	default: //list
		break;
}

/* actual output */
echo $oTWIG->render( 
	$template_name, // template-filename
    $oFC->page_content // template-data
);

if ($oFC->setting [ 'debug' ] == "yes" ){
	Gsm_debug ($oFC->page_content, __LINE__ . $template_name );  
	if (LOAD_MODE == "x" )  Gsm_debug ($oFC->version, $template_name );
}
?>