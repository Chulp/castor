<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2025 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php
 
// start
$module_name = 'vedit';
$version = '20250118';
$project = "Document Archive Editing";
$main_file = "castor";
$default_template = '/edit.lte';

/* start initialize module */
global $oLEPTON;
$oFC = gsmoffa::getInstance();
$oTWIG = lib_twig_box::getInstance ( );
$oTWIG-> registerModule ( LOAD_MODULE . LOAD_SUFFIX );
$template_name= '@' . LOAD_MODULE . LOAD_SUFFIX . "/". LANGUAGE . $default_template;

/* version data */
$oFC->version [ $module_name ] = $version;
$oFC->version = array_merge ( $oFC->version, $version_display);
$oFC->version [ $oFC-> language [ 'LANG' ] ] = $oFC-> language [ 'VERS' ];
$oFC->version [ "set" ] = $FC_SET [ 'version' ] ;

/* file references */
$oFC->file_ref  [ 99 ] = LOAD_DBBASE . "_".$main_file;

/* settings */
$oFC->setting [ 'includes' ] 		= $place [ 'includes' ];
$oFC->setting [ 'frontend' ] 		= $place [ 'frontend' ];
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET );
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "zoek" );
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "entity");

/* default values */
$oFC->page_content [ 'FORMULIER' ] = $project;
$oFC->page_content [ 'MODULE' ] = $module_name;
$oFC->page_content [ 'MODE' ] = 9;
$oFC->page_content [ 'PAGE_ID' ] = $page_id ?? 0;
$oFC->page_content [ 'SECTION_ID' ] = $section_id ?? 0;

/* Gebruik limited door niet rechthebbenden */
if ( $oFC->user [ 'privileged' ] > 0 ) {
	$oFC->user  = array_merge (	$oFC->user, $oFC->gsm_adresDet ( $oFC->page_content [ 'PAGE_ID' ], $oFC->setting [ 'owner' ] ) );
	if ($oFC->user[ 'privileged' ] > 2 ) {
		$oFC->page_content [ 'MODE' ] = 9;
		$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET );
	}
	/* niet genoeg rechten */
	if ( $oFC->page_content [ 'MODE' ] == 0 ) {
		$oFC->description = $oFC->language [ 'TXT_NO_ACCESS' ];
		unset ( $_POST[ 'command' ] );
	}
	/* end genoeg rechten */
}

/* create condition for sips test */
$_SESSION[ 'page_h' ] = $oFC->page_content ['HASH'];

/* get memory values */ 
$oFC->gsm_memorySaved ( );

if ($oFC->setting [ 'debug' ] == "yes" ) Gsm_debug (array ("post"=> $_POST, "get"=>$_GET ?? "", "this"=>$oFC, $selection ?? ""), __LINE__ . __FUNCTION__ ); 

/* Input processing */
if ( isset( $_POST[ 'command' ] ) ) {
	switch ( $_POST[ 'command' ] ) {
		case "up":
			$oFC->page_content [ 'POSITION' ] = $oFC->gsm_pagePosition ("up", $oFC->page_content [ 'POSITION' ] , $_POST[ 'n2' ],  $oFC->setting [ 'qty_max' ], $_POST[ 'n0' ], $_POST[ 'n1' ] );
			break;
		case "down":
			$oFC->page_content [ 'POSITION' ] = $oFC->gsm_pagePosition ("down", $oFC->page_content [ 'POSITION' ] , $_POST[ 'n2' ], $oFC->setting [ 'qty_max' ], $_POST[ 'n0' ], $_POST[ 'n1' ] );
			break;
		case "View":
			break;
		case "Reset":
			$oFC->recid = '';
			unset ( $_POST [ 'zoek_pf' ] );
			unset ( $_POST [ 'zoek_pr' ] );
            unset ( $_POST [ 'zoek_bd' ] );
            unset ( $_POST [ 'zoek_sn' ] );
            unset ( $_POST [ 'zoek_ft' ] );
			$selection= "";
			$oFC->page_content [ 'PARAMETER' ] = $selection;
			$oFC->selection = $selection;
			$oFC->page_content [ 'SUB_HEADER' ]= "____";
			break;
		case "Proces":			
		case "Save":
			$PostArr = array();
			$DATfilter  = sprintf ("y{%s;%s;%s}", 
				date( "Y-m-d", time() ), 
				date( "Y", time() )-100 . "-01-01", 
				date( "Y", time() ) +2  . "-12-31"); 
			foreach ($_POST as $key => $value ) { 
				$localHulpA = explode ("|", $key);
				if ( isset ( $localHulpA [0] ) && 
					$localHulpA [ 0 ] == "a" && 
					isset ( $localHulpA [ 2 ] ) && 
					$localHulpA[2] != $value ) { 
					$PostArr [ $localHulpA [ 1 ] ] [ 1 ] = $oFC->gsm_sanitizeStringD ( $value, $DATfilter );
				}
				if ( isset ( $localHulpA [ 0 ] ) && 
					$localHulpA [ 0 ] == "b" && 
					isset($localHulpA [ 2 ] ) && 
					$localHulpA [ 2 ] != $value ) { 
					$PostArr [ $localHulpA [ 1 ] ] [ 2 ] = $oFC->gsm_sanitizeStringS( $value,
					"s{STRIP|TOASC|LOWER|CLEAN}");
				}
				if ( isset ( $localHulpA [ 0 ] ) && 
					$localHulpA [ 0 ] == "c" && 
					isset ( $localHulpA [ 2 ] ) && 
					$localHulpA [ 2 ] !== $value ) { 
					$PostArr [ $localHulpA [ 1 ] ] [ 3 ] = $oFC->gsm_sanitizeStringS( $value,
					"s{STRIP|TOASC|LOWER|CLEAN}" );
				}
				if ( isset ( $localHulpA [ 0 ] ) && 
					$localHulpA [ 0 ] == "d" && 
					isset ( $localHulpA [ 2 ] ) && 
					$localHulpA [ 2 ] != str_replace (" ", "_", $value ) ) { 
					$PostArr [ $localHulpA [ 1 ] ] [ 4 ] = $oFC->gsm_sanitizeStringS( $value,
					"s{STRIP|TOASC|LOWER|CLEAN}" );
				}
			}
			if ( count ( $PostArr ) > 0 ) {
				foreach ($PostArr as $key => $value ) { 
					$check_query  = "SELECT * FROM `" . $oFC->file_ref [ 99 ]."` WHERE `id` = '".$key."' ";
					$check_result = array ();
					if ( $database->execute_query( $check_query, true, $check_result ) && count ( $check_result ) > 0) {
						$result = current ( $check_result );
						$location = LEPTON_PATH. $result[ 'area' ] . $result[ 'location' ] . $result[ 'name' ]; 
						if ( file_exists ( $location ) ) {
							$updateArr = array();
							if ( isset ( $PostArr [ $key ] [ 1 ] ) ) {
								$updateArr [ 'date' ] = $PostArr [ $key ] [ 1 ];
								$restult [ 'date' ] = $updateArr [ 'date' ];
							}
							if ( isset ( $PostArr [ $key ] [ 2 ] ) ) {
								$updateArr [ 'type' ] = strtoupper ( $PostArr [ $key ] [ 2 ]);
								$restult [ 'type' ] = $updateArr [ 'type' ];
							}
							if ( isset ( $PostArr [ $key ] [ 3 ] ) ) {
								$updateArr [ 'ref' ] = $PostArr [ $key ] [ 3 ] ;
								$restult [ 'ref' ] = $updateArr [ 'ref' ];
							}
							if ( isset ( $PostArr [ $key ] [ 4 ] ) ) {
								$updateArr [ 'keywords' ] = $PostArr [ $key ] [ 4 ] ;
								$restult [ 'keywords' ] = $updateArr [ 'keywords' ];
							}
							$result['namec'] =sprintf ("%s%s_%s%s_%s.%s",
								( isset ( $updateArr [ 'type' ] ) ) 
									? strtoupper ( $updateArr [ 'type' ]) 
									: strtoupper ( $result [ 'type' ]),
								( isset ( $updateArr [ 'ref' ] ) ) ? $updateArr [ 'ref' ] : $result [ 'ref' ],
								( isset ( $updateArr [ 'date' ] ) ) ? $updateArr [ 'date' ] : $result [ 'date' ],
								$result [ 'hash' ],
								( isset ( $updateArr [ 'keywords' ] ) ) ? str_replace (" ", "_",$updateArr [ 'keywords' ] ) : $result [ 'keywords' ],
								$result [ 'filetype' ] );
							$location_new = LEPTON_PATH. $result['area']. $result['location']. $result['namec'];
							if ( count ( $updateArr ) > 0 && !file_exists ( $location_new ) ) {
								// naam bijwerken 
								$updateArr [ 'name' ] = $result [ 'namec' ];
								// trail bijwerken
								if (strlen ( $result [ 'content_short' ] ?? "0" ) < 3 ) $updateArr [ 'content_short' ] = $result [ 'name' ] ;
								// zoek bijwerken
								$arout = $oFC->setting [ 'zoek' ] [ 'castor' ];
								// zoek correct ??
								foreach ( $result as $pay => $load ) $arout = str_replace ( "|".$pay."|", "|".$load."|", $arout );
								$arout = str_replace ( "||", "|", $arout );			
								$updateArr ['zoek'] = strtolower ( $oFC->gsm_sanitizeStrings ( $arout, 's{TOASC|LOWER}' ) ); 
								// update
								$database->build_and_execute(
									"update",
									$oFC->file_ref [ 99 ],
									$updateArr,
									"`id` = '" . $key . "'");
								rename ($location, $location_new);
								$oFC->description .= ' renamed : '. $result ['namec' ] . NL;
							}
						}
					}
				}
			}
			break;
		case "Select":
		default:
			$oFC->page_content [ 'MODE' ] = 9;
			unset ( $_POST [ 'zoek_pf' ] );
            unset ( $_POST [ 'zoek_ft' ] );
			break;
	} 
} elseif ( isset( $_GET[ 'command' ] ) ) {
	switch ( $_GET[ 'command' ] ) {
		case 'select': //    1=>'Wijzigen', 
			// is a record selected ? 
			if ( !isset( $_GET[ 'recid' ] ) ) {
				$oFC->page_content [ 'MODE' ] = 9;
				$oFC->description .= $oFC->language ['TXT_ERROR_DATA'];
				break;
			} 
			// yes	  
			$oFC->recid = $_GET[ 'recid' ];
			$oFC->page_content [ 'MODE' ] = 8;
			break;
		default:
			// escape route 
			$oFC->page_content [ 'MODE' ] = 9;
			break;
	} 
} else {
	// so standard display / first run
}

if ( isset ( $selection ) ) {
	$selection = $oFC->gsm_ParameterEval ( $selection , "castor" );
	$oFC->page_content [ 'PARAMETER' ] = $selection;
	$oFC->page_content [ 'SUB_HEADER' ] = strtoupper ( $oFC->page_content[ 'PARAMETER' ] );
	$oFC->search_mysql = $oFC->gsm_ParameterSearch ( $selection, "castor" );
}

// display preparation
switch ( $oFC->page_content [ 'MODE' ] ) {
	case 0:
		$oFC->setting [ 'entity' ] = array ( $oFC->setting [ 'owner' ]  ) ;  // beperkte toegang
		break;
	default: // default list
		$pageok = true;
		// bepaal aantal records
		$result = array ( );
		$query = "SELECT count(`id`) FROM `" . $oFC->file_ref [ 99 ] . "` ";
		$query .= $oFC->search_mysql;
		$database->execute_query( $query, true, $result );
		$row = current ( $result );
		$oFC->page_content [ 'aantal' ] = $row [ "count(`id`)" ];
		$limit_sql = $oFC->gsm_pagePosition ("sql", $oFC->page_content [ 'POSITION' ] , $oFC->page_content [ 'aantal' ], $oFC->setting [ 'qty_max' ]  );
		$query  = "SELECT * FROM `" . $oFC->file_ref [ 99 ] . "` ";
		$query .= $oFC->search_mysql;
		$query .= " ORDER BY  `id` DESC ". $limit_sql;
		$results = array();
		$job = array();
		if ( $database->execute_query( $query, true, $results) && count ( $results ) > 0 ) { 
			$cal = array();
			foreach ( $results as  $row ) {
				$row ['namec'] = sprintf ("%s%s %s %s",
					$row['type'], 
					$row['ref'], 
					$row['date'], 
					$row['keywords'], 
					$row['filetype']);
				$row ['link'] = sprintf ("%s%s%s%s",
					LEPTON_URL,
					$row ['area'], 
					$row ['location'], 
					$row ['name']);
				$row ['TYPE' ] = $oFC->gsm_selectOption ( $oFC->setting [ 'entity' ], $row [ 'type' ], 2);
				switch ( $row['filetype'] ) {
					case "jpg":
						$row ['toon'] = sprintf ('<a href="%1$s%2$s%3$s%4$s%5$s"><img src="%1$s%2$s%3$s%4$s%5$s" width="500" height=" " /></a>',
							LEPTON_URL,
							$oFC->subdir ?? "",
							$row["area"], 
							$row["location"], 
							$row["name"],
							$row["namec"]);
						break;
					case "pdf":
						$row ['toon'] = sprintf ('<center><embed height="400" width="580" src="%1$s%2$s%3$s%4$s%5$s"#toolbar=1&navpanes=0&scrollbar=1"></embed></center>', 
							LEPTON_URL,
							$oFC->subdir ?? "",
							$row["area"], 
							$row["location"], 
							$row["name"],
							$row["namec"]);
						break;
					default:
						$row ['toon'] = sprintf ('<a href="%1$s%2$s%3$s%4$s%5$s">%6$s</a>', 
							LEPTON_URL,
							$oFC->subdir ?? "",
							$row["area"], 
							$row["location"], 
							$row["name"],
							$row["namec"]);
						break;
				}
				$cal []= $row;
			}
		} else {
			$oFC->page_content[ 'STATUS_MESSAGE' ] .= $oFC->language [ 'TXT_ERROR_DATA' ].NL;
		}
		if ( isset ($cal ) ) $oFC->page_content [ 'RESULTS' ] = $cal;
		break;
}

/*
 * the selection options
 */
 
switch ( $oFC->page_content [ 'MODE' ] ) {
	case 0:
		$oFC->page_content [ 'SELECTION' ] = "";
		break;
	default: 
		if ( !is_array ( $oFC->setting [ 'entity' ] ) ) $oFC->setting [ 'entity' ] = explode ("|" , $oFC->setting [ 'entity' ] );
		if ( is_array ( $oFC->setting [ 'entity' ] ) ) array_unshift ( $oFC->setting [ 'entity' ], " " );
		$oFC->page_content[ 'GSM_TYPE' ] = $oFC->gsm_selectOption ( $oFC->setting [ 'entity' ], ' ', 2);
		array_unshift ( $oFC->setting [ 'allowed' ], " " );
		$oFC->page_content[ 'GSM_FILETYPE' ] = $oFC->gsm_selectOption ( $oFC->setting [ 'allowed' ], '-', 3);
		
		$oFC->page_content [ 'SELECTIONC' ] = $oFC->gsm_opmaakSel ( array ( 1, 6 ) );
		$oFC->page_content [ 'SELECTIONE' ] = $oFC->gsm_opmaakSel ( array ( 7 ) );		
//		$oFC->page_content [ 'SELECTIONB' ] = $oFC->gsm_opmaakSel ( array ( 10 ) );
		$oFC->page_content [ 'SELECTIONC' ] = $oFC->gsm_opmaakSel ( array ( 1, 6 ) );
		if ( $oFC->page_content [ 'aantal' ] > $oFC->setting [ 'qty_max' ] ) 
			$oFC->page_content [ 'SELECTIONB' ] = $oFC->gsm_opmaakSel ( array ( 13), "-", "-", $oFC->page_content [ 'POSITION' ], $oFC->page_content [ 'aantal' ] , $oFC->setting [ 'qty_max' ]  );
//		$oFC->page_content [ 'SELECTIONA' ] = $oFC->gsm_opmaakSel ( array ( 1, 6 ) );
//		$oFC->page_content [ 'SELECTION' ] = $oFC->page_content['SELECTIONC'] . $oFC->page_content [ 'SELECTIONB' ] . $oFC->page_content [ 'SELECTIONE' ];
		break;
}

/* output processing */
/* memory save */
$oFC->page_content ['MEMORY'] = $oFC->gsm_memorySaved ( 2 ); 

/* als er boodschappen zijn deze tonen in een error blok */
$oFC->page_content[ 'STATUS_MESSAGE' ] .= $oFC->description; 
if (strlen($oFC->page_content[ 'STATUS_MESSAGE' ])>4 ) $oFC->page_content['MESSAGE_CLASS']= "ui error message"; 
$oFC->page_content[ 'VERSIE' ] = $oFC->version; 
$oFC->page_content [ 'MODE' ] = $oFC->page_content [ 'MODE' ];
if (LOAD_MODE == "x" )  $_SESSION[ 'last_edit_section' ] = $section_id; 

switch ( $oFC->page_content [ 'MODE' ] ) {
	default: 
		break;
}

/* actual output */
echo $oTWIG->render( 
	$template_name, // template-filename
    $oFC->page_content // template-data
);

if ($oFC->setting [ 'debug' ] == "yes" ){
	Gsm_debug ($oFC->page_content, __LINE__ . $template_name );  
	if (LOAD_MODE == "x" )  Gsm_debug ($oFC->version, $template_name );
}
?>