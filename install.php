<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2025 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php

/* create the empty directories */
if ( !file_exists ( LEPTON_PATH . MEDIA_DIRECTORY . '/archive' ) ) {
  echo (LEPTON_tools::display('media/archive directory for new uploads</b>','pre','ui positive message')); 
  mkdir ( LEPTON_PATH . MEDIA_DIRECTORY . '/archive' ); 	// create directory for new uploads
} else {
  echo (LEPTON_tools::display('oeps cmedia/archive directory existed already</b>','pre','ui negative message'));  
}

if ( !file_exists ( LEPTON_PATH . '/castor' ) ) {
  echo (LEPTON_tools::display('castor directory for archiving documents</b>','pre','ui positive message')); 
  mkdir ( LEPTON_PATH . '/castor' ); 		// create directory for archiving documents 
} else {
  echo (LEPTON_tools::display('oeps castor directory existed already</b>','pre','ui negative message')); 
}

$droplet_names = array();
if ( file_exists ( dirname (__FILE__) . '/install/droplet_Gsm_castordoc.zip'))	$droplet_names [] = 'droplet_Gsm_castordoc';
if ( file_exists ( dirname (__FILE__) . '/install/droplet_Gsm_mediadoc.zip'))	$droplet_names [] = 'droplet_Gsm_mediadoc';
if ( file_exists ( dirname (__FILE__) . '/install/droplet_Gsm_mediadocIn.zip'))	$droplet_names [] = 'droplet_Gsm_mediadocIn';

if ( count ( $droplet_names ) >0 ) {
  echo (LEPTON_tools::display('(re-)install droplets</b>','pre','ui positive message')); 
  LEPTON_handle::install_droplets ( 'gsmoffa', $droplet_names);
}

echo (LEPTON_tools::display('to complete install: run backend setupa function to create the database and actvate the modules</b>','pre','ui message')); 
echo (LEPTON_tools::display('use D_REPAIR_ setup the database.</b>','pre','ui message')); 
echo (LEPTON_tools::display('use D_INSTALL_ to (re-)install frontend functionality.</b>','pre','ui message')); 

?>