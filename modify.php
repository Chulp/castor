<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2025 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php
 
/*
 *  This is the office module backend loader
 */
 
/* initial settings */
$version_display [ 'modify' ] = '20250118';
define( 'LOAD_MODE',  'x' );
$LOAD_function = ( LOAD_MODE == "x" ) ? 'function' : 'menu'; 

/* on-line off line */
// some editors are frustrating the next few lines  !!!!
$LOAD_help = explode ( "\\", str_replace ( LEPTON_PATH, "", dirname ( __FILE__ ) ) );
if ( !isset ( $LOAD_help [ 2 ] ) ) { 
	$LOAD_help = explode ( "/", str_replace ( LEPTON_PATH, "", dirname ( __FILE__ ) ) ); 
	define ( 'LOAD_OLINE', true ); // on-line
} else {
	define ( 'LOAD_OLINE', false );
}
/* determine the package module and suffix */
// 1char suffix. ( for 2 char suffix replace -1 by -2 in next two lines)
define ( 'LOAD_MODULE', substr ( $LOAD_help [ 2 ], 0, -1 ) );
define ( 'LOAD_SUFFIX', substr ( $LOAD_help [ 2 ], -1 ) );

/* fixed values and settings */
define( 'LOAD_DBBASE', TABLE_PREFIX . 'mod_go' );
date_default_timezone_set( 'Europe/Paris' );
define ( 'NL', '<br/>' );

/* return path */
$page_id    = ( isset( $page_id ) ) ? $page_id : 0;
$section_id = ( isset( $section_id ) ) ? $section_id : 0;
define( "LOAD_RETURN", "modify.php?section_id=" . $section_id . "&page_id=" . $page_id );

/* directories */
global $place;
$place = array ();
$place [ 'includes' ] = ( dirname ( __FILE__ ) ) . '/';

/* frontend if index.php can be found in frontend template  */
$place [ 'frontend' ] = (file_exists  ( LEPTON_PATH . '/templates/' . DEFAULT_TEMPLATE . '/frontend/' . LOAD_MODULE . LOAD_SUFFIX . '/index.php' ) ) 
	? LEPTON_PATH . '/templates/' . DEFAULT_TEMPLATE . '/frontend/' . LOAD_MODULE . LOAD_SUFFIX . '/'
	: $place ['includes'] ;
	
/* load the SET modules */
$place [ 'settings' ]  = $place[ 'frontend' ]. 'SET.php';
if ( file_exists( $place[ 'settings' ] ) ) require_once( $place [ 'settings' ] );

/* Functions */
function Gsm_prout ( $template, $parseArray ) {
	$returnvalue = $template;
	foreach ( $parseArray as $pay => $load ) { $returnvalue = str_replace( "{" . $pay . "}", $load, $returnvalue ); } 
	return $returnvalue;
}

function Gsm_option( $input, $select = '' ) {
	$TEMPL_F [ 1 ] = '<option value="%s" %s>%s'."\n"; $n = 1; // max # selected
	$returnvalue = '';
	if ( $select == '' ) $n = 0;
	foreach ( $input as $pay => $load ) {
		if ( $n > 0 && $select == $pay ) { $returnvalue .= sprintf ( $TEMPL_F [ 1 ], $pay, 'selected', $load );  $n--; 
		} else { $returnvalue .= sprintf( $TEMPL_F[ 1 ], $pay, '', $load ); }
	}
	return $returnvalue;
}

function Gsm_debug ( $input, $line = __LINE__ .  __FUNCTION__  ) {	
	$returnvalue = "";
	if (LOAD_MODE == "v") {
		echo '<div class="ui accordion"><div class="title"><i class="dropdown icon"></i>log:' . $line . '</div><div class="content "><pre>';
	} else {
		echo '<div class="title"><i class="dropdown icon"></i>log:' . $line . '</div><div class="content "><pre>';}
	print_r ($input);
	if (LOAD_MODE == "v") {
		echo NL.'</pre></div></div>';
	} else {
		echo NL.'</pre></div>';}
	return $returnvalue;
}

function Gsm_debugc ($input, $line = __LINE__ .  __FUNCTION__  ) {
	if ( is_array( $input ) ) { 
		$output = "";
		foreach ($input as $key => $value ) {
			if ( is_array ( $value ) ) { 
				foreach ($value as $key1 => $value1 ) $output .= $key . " / " .$key1 . " => " . $value1 ." | ";   
			} else { $output .= $key ." => " . $value ." | "; 
		}	}
	} else { $output = $input; }
	echo "<script>console.log ( '" . $line . " => " . $output . "' );</script>";
} 

/* check section function */
if (isset( $FC_SET [ 'SET_' . $LOAD_function ] ) ) {  
	$LOAD_help = explode ( "|", $FC_SET [ 'SET_' .  $LOAD_function ] ); 
	foreach ( $LOAD_help  as $pay => $load ) { if ( strlen ( trim ( $load ) ) >1 ) $LOAD_menu [ LOAD_MODE.strtolower ( $load ) ] = strtolower ( $load ) ;  }
} else { 
	$LOAD_menu [ LOAD_MODE . 'dummy' ] = "------"; 
}

/* Get menu input if any */
$LOAD_module = ( isset ( $_GET[ 'module' ] ) ) ? strtolower ( $_GET[ 'module' ] ) : "";
if ( isset ( $_POST [ 'module' ] ) ) $LOAD_module = strtolower ( $_POST [ 'module' ]);
if ( substr ( $LOAD_module, 0, 1) != LOAD_MODE ) $LOAD_module = LOAD_MODE . $LOAD_module;
$selection = ( isset ( $_GET[ 'selection' ] ) )  ? strtolower ( $_GET[ 'selection' ] ) : "";
if ( isset ( $_POST[ 'selection' ] ) ) $selection = strtolower ( $_POST[ 'selection' ] );

/* additional functions */
$xmode = "";
if ( substr ( $selection, 0, 1 ) == "?" ) { 
	$xmode .= "EXPL ";
	$selection = substr( $selection, 1 ); } 
if ( substr ( $selection, 0, 6) == "debug_" ) { $debug = true; $selection = str_replace( "debug_", "d_", $selection ); }
if ( substr ( $selection, 0, 2 ) == "d_" ) { $LOAD_help = explode ( "_" , $selection );
	$xmode .= ( isset ( $LOAD_help [ 1 ] ) ) ? strtoupper( $LOAD_help [1] ) : '' ;
	$selection = ( isset ( $LOAD_help [2] ) ) ? $LOAD_help [ 2 ] : ' '; }

/* template for submenu  */
$TEMPLATE ['modify'] = 
	'<div class="container">
		<div class="ui blue segment">
			<form class="ui form" name="menu" method="post" action="{return}">
				<div class="fields">
					<div class="field">
						<input class="ui grey button" type="submit" value="Select" />
					</div>
					<div class="field">
						<select name="module" >{module}</select></td>
					</div>
					<div class="field">
						<input type="text" name="selection" value="{parameter}" placeholder="Parameter" />
					</div>
				</div>
			</form>
		</div>  
	</div>';  

/* submenu display needed */
if (count( $LOAD_menu ) == 1) { 
	if ( strlen ( $LOAD_module ) < 3 ) { foreach ( $LOAD_menu  as $pay => $load ) $LOAD_module = $pay; }
} else {
	foreach ( $LOAD_menu as $pay => $load ) 
		$set_menu [ $pay ] = (isset($FC_SET ['SET_txt_menu'][$pay])) ? $FC_SET ['SET_txt_menu'][$pay] : $pay; 
	$parseViewArray = array(
		'return' 	=> LOAD_RETURN, 
		'parameter' => $selection,
		'module' 	=> Gsm_option( $set_menu, $module ),
		'mod' 		=> $module);
	echo Gsm_prout ( $TEMPLATE ['modify'], $parseViewArray ) ;
}

/* load the module  */
if ( strlen ( $LOAD_module ) > 2 ) {
	require_once ( file_exists ( $place [ 'includes' ] . $LOAD_module . '.php' )
 		? $place [ 'includes' ] . $LOAD_module . '.php'
		: $place [ 'includes' ] . LOAD_MODE . 'dummy.php');
}
?>